# Locate x265 libx265 libx265.so libx265.a library
# This module defines
# x265_LIBRARIES, the name of the library to link against
# x265_FOUND, if false, do not try to link to AVFormat
# x265_INCLUDE_DIRS, where to find headers


find_path(x265_INCLUDE_DIRS x265.h
  HINTS $ENV{x265_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks  /Library/Frameworks
    /usr/local/include  /usr/include
    /sw/include
    /opt/local/include /opt/csw/include /opt/include
    /mingw/include)

find_library(x265_LIBRARIES
  NAMES x265 libx265 libx265.so libx265.a
  HINTS $ENV{x265_HOME}
  PATH_SUFFIXES lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(x265
  FOUND_VAR x265_FOUND
  REQUIRED_VARS x265_LIBRARIES x265_INCLUDE_DIRS
)
