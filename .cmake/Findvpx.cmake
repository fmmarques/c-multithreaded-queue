# Locate vpx libvpx libvpx.so libvpx.a library
# This module defines
# vpx_LIBRARIES, the name of the library to link against
# vpx_FOUND, if false, do not try to link to AVFormat
# vpx_INCLUDE_DIRS, where to find headers


find_path(vpx_INCLUDE_DIRS vpx/vpx_codec.h
  HINTS $ENV{vpx_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks  /Library/Frameworks
    /usr/local/include  /usr/include
    /sw/include
    /opt/local/include /opt/csw/include /opt/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
    /mingw/include)

find_library(vpx_LIBRARIES
  NAMES vpx libvpx libvpx.so libvpx.a
  HINTS $ENV{vpx_HOME}
  PATH_SUFFIXES lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
    /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(vpx
  FOUND_VAR vpx_FOUND
  REQUIRED_VARS vpx_LIBRARIES vpx_INCLUDE_DIRS
)
