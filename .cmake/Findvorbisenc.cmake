# Locate libvorbisenc library
# This module defines
# vorbisenc_LIBRARY, the name of the library to link against
# vorbisenc_FOUND, if false, do not try to link to AVFormat
# vorbisenc_INCLUDE_DIR, where to find avUtil.h
find_path(vorbisenc_INCLUDE_DIR vorbis/vorbisenc.h
  HINTS
  $ENV{vorbisenc_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks	   /Library/Frameworks
    /usr/local/include	   /usr/include
    /sw/include
    /opt/local/include	   /opt/csw/include	/opt/include
    /mingw/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
)

find_library( 
    vorbisenc_LIBRARY
    NAMES		  libvorbisenc.so libvorbisenc.a
    HINTS		  $ENV{vorbisenc_HOME}
    PATH_SUFFIXES	  lib64	lib bin
    PATHS
      /usr/local	  /usr	    /usr/lib/x86_64-linux-gnu
      /sw
      /opt/local	  /opt/csw  /opt
      /mingw
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(vorbisenc 
 FOUND_VAR vorbisenc_FOUND
 REQUIRED_VARS vorbisenc_LIBRARY vorbisenc_INCLUDE_DIR
)
