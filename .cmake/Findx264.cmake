# Locate x264 libx264 libx264.so libx264.a library
# This module defines
# x264_LIBRARIES, the name of the library to link against
# x264_FOUND, if false, do not try to link to AVFormat
# x264_INCLUDE_DIRS, where to find headers


find_path(x264_INCLUDE_DIRS x264.h
  HINTS $ENV{x264_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks  /Library/Frameworks
    /usr/local/include  /usr/include
    /sw/include
    /opt/local/include /opt/csw/include /opt/include
    /mingw/include)

find_library(x264_LIBRARIES
  NAMES x264 libx264 libx264.so libx264.a
  HINTS $ENV{x264_HOME}
  PATH_SUFFIXES lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(x264
  FOUND_VAR x264_FOUND
  REQUIRED_VARS x264_LIBRARIES x264_INCLUDE_DIRS
)
