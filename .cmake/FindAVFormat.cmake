# Locate AVFormat library
# This module defines
# AVFormat_LIBRARY, the name of the library to link against
# AVFormat_FOUND, if false, do not try to link to AVFormat
# AVFormat_INCLUDE_DIR, where to find avUtil.h
#



find_path( AVFormat_INCLUDE_DIRS libavformat/avformat.h
  HINTS
  $ENV{AVFORMAT_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks	   /Library/Frameworks
    /usr/local/include	   /usr/include
    /sw/include
    /opt/local/include	   /opt/csw/include	/opt/include
    /mingw/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
)

find_library(AVFormat_LIBRARIES
  NAMES avformat libavformat.so libavformat.a
  HINTS $ENV{AVFORMAT_HOME}
  PATH_SUFFIXES lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
    /sw /opt/local /opt/csw /opt /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(AVFormat
  FOUND_VAR AVFormat_FOUND
  REQUIRED_VARS AVFormat_LIBRARIES AVFormat_INCLUDE_DIRS
)
