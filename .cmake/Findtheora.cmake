# Locate theora libtheora libtheora.so libtheora.a library
# This module defines
# theora_LIBRARIES, the name of the library to link against
# theora_FOUND, if false, do not try to link to AVFormat
# theora_INCLUDE_DIRS, where to find headers


find_path(theora_INCLUDE_DIRS theora/theora.h
  HINTS $ENV{theora_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks  /Library/Frameworks
    /usr/local/include  /usr/include
    /sw/include
    /opt/local/include /opt/csw/include /opt/include
    /mingw/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
)
find_library(theora_LIBRARIES
  NAMES theora libtheora libtheora.so libtheora.a
  HINTS $ENV{theora_HOME}
  PATH_SUFFIXES lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    /mingw
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
)

if(theora_INCLUDE_DIRS)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(theora
  FOUND_VAR theora_FOUND
  REQUIRED_VARS theora_LIBRARIES theora_INCLUDE_DIRS
)
endif()
