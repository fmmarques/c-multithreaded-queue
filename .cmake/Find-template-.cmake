# Locate %LIB_NAME% library
# This module defines
# %LIB_PREFIX%_LIBRARIES, the name of the library to link against
# %LIB_PREFIX%_FOUND, if false, do not try to link to AVFormat
# %LIB_PREFIX%_INCLUDE_DIRS, where to find headers


find_path(%LIB_PREFIX%_INCLUDE_DIRS %LIB_HEADER%
  HINTS $ENV{%LIB_PREFIX%_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks  /Library/Frameworks
    /usr/local/include  /usr/include
    /sw/include
    /opt/local/include /opt/csw/include /opt/include
    /mingw/include)

find_library(%LIB_PREFIX%_LIBRARIES
  NAMES %LIB_NAME%
  HINTS $ENV{%LIB_DIR%}
  PATH_SUFFIXES lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(%LIB_PREFIX%
  FOUND_VAR %LIB_PREFIX%_FOUND
  REQUIRED_VARS %LIB_PREFIX%_LIBRARIES %LIB_PREFIX%_INCLUDE_DIRS
)
