# Locate AVUtil library
# This module defines
# AVUtil_LIBRARY, the name of the library to link against
# AVUtil_FOUND, if false, do not try to link to AVUtil
# AVUtil_INCLUDE_DIR, where to find avUtil.h
#


find_path( AVUtil_INCLUDE_DIRS libavutil/avutil.h
  HINTS
  $ENV{AVUTIL_HOME}
  PATH_SUFFIXES include 
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local/include
  /usr/include
  /sw/include
  /opt/local/include
  /opt/csw/include 
  /opt/include
  /mingw/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
)

find_library( AVUtil_LIBRARIES
  NAMES	      avutil libavutil.a libavutil.so
  HINTS	      $ENV{AVUTIL_HOME}
  PATH_SUFFIXES lib64 lib bin
  PATHS /usr/local  /usr      /sw
	/opt/local  /opt/csw  /opt	
	/mingw
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(AVUtil 
                                  FOUND_VAR AVUtil_FOUND
                                  REQUIRED_VARS AVUtil_LIBRARIES AVUtil_INCLUDE_DIRS)
