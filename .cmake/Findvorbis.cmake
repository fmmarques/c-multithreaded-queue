# Locate libvorbis library
# This module defines
# vorbis_LIBRARY, the name of the library to link against
# vorbis_FOUND, if false, do not try to link to AVFormat
# vorbis_INCLUDE_DIR, where to find avUtil.h

find_path(vorbis_INCLUDE_DIRS vorbis/codec.h
  HINTS $ENV{vorbis_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks /Library/Frameworks
    /usr/local/include /usr/include
    /sw/include
    /opt/local/include /opt/csw/include	/opt/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
    /mingw/include)

find_library(vorbis_LIBRARIES
  NAMES libvorbis.so libvorbis.a
  HINTS $ENV{vorbis_HOME}
  PATH_SUFFIXES	  lib64	lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
    /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(vorbis
  FOUND_VAR vorbis_FOUND
  REQUIRED_VARS vorbis_LIBRARIES vorbis_INCLUDE_DIRS
)

