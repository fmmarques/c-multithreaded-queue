# Locate libopus library
# This module defines
# opus_LIBRARY, the name of the library to link against
# opus_FOUND, if false, do not try to link to AVFormat
# opus_INCLUDE_DIR, where to find avUtil.h

find_path(opus_INCLUDE_DIRS opus/opus.h
  HINTS $ENV{opus_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks /Library/Frameworks
    /usr/local/include	/usr/include
    /sw/include
    /opt/local/include	/opt/csw/include /opt/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
    /mingw/include)

find_library( 
  opus_LIBRARIES
  NAMES	opus libopus.so libopus.a
  HINTS	$ENV{opus_HOME}
  PATH_SUFFIXES	lib64 lib bin
  PATHS
    /usr/local /usr /usr/lib/x86_64-linux-gnu
    /sw
    /opt/local /opt/csw /opt
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
    /mingw)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(opus
  FOUND_VAR opus_FOUND
  REQUIRED_VARS opus_LIBRARIES opus_INCLUDE_DIRS)
