# Locate libmp3lame library
# This module defines
# mp3lame_LIBRARY, the name of the library to link against
# mp3lame_FOUND, if false, do not try to link to AVFormat
# mp3lame_INCLUDE_DIR, where to find avUtil.h

set(mp3lame_FOUND off)

find_path(mp3lame_INCLUDE_DIRS lame/lame.h
  HINTS
  $ENV{mp3lame_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks /Library/Frameworks
    /usr/local/include /usr/include
    /sw/include
    /opt/local/include /opt/csw/include	/opt/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
    /mingw/include)

find_library( 
    mp3lame_LIBRARIES
    NAMES mp3lame libmp3lame.so libmp3lame.a
    HINTS $ENV{mp3lame_HOME}
    PATH_SUFFIXES lib64 lib bin
    PATHS
      /usr/local /usr /usr/lib/x86_64-linux-gnu
      /sw
      /opt/local /opt/csw /opt
      /mingw
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(mp3lame
  FOUND_VAR mp3lame_FOUND
  REQUIRED_VARS mp3lame_LIBRARIES mp3lame_INCLUDE_DIRS
)
