# Locate libswresample library
# This module defines
# swresample_LIBRARY, the name of the library to link against
# swresample_FOUND, if false, do not try to link to AVFormat
# swresample_INCLUDE_DIR, where to find avUtil.h

find_path(swresample_INCLUDE_DIR mp3lame.h
  HINTS
  $ENV{swresample_HOME}
  PATH_SUFFIXES include 
  PATHS
    ~/Library/Frameworks	   /Library/Frameworks
    /usr/local/include	   /usr/include
    /sw/include
    /opt/local/include	   /opt/csw/include	/opt/include
    /mingw/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
)

find_library( 
    swresample_LIBRARY
    NAMES		  libswresample
    HINTS		  $ENV{swresample_HOME}
    PATH_SUFFIXES	  lib64	lib bin
    PATHS
      /usr/local	  /usr	    /usr/lib/x86_64-linux-gnu
      /sw
      /opt/local	  /opt/csw  /opt
      /mingw
)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(swresample
                                  FOUND_VAR swresample_FOUND
                                  REQUIRED_VARS swresample_LIBRARIES swresample_INCLUDE_DIRS
)
