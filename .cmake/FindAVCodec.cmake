# Locate AVCodec library
# This module defines
# AVCodec_LIBRARIES, the name of the library to link against
# AVCodec_FOUND, if false, do not try to link to AVCodec
# AVCodec_INCLUDE_DIRS, where to find avCodec.h
#


find_path( AVCodec_INCLUDE_DIRS libavcodec/avcodec.h
  HINTS $ENV{AVCODEC_HOME}
  PATH_SUFFIXES include
  PATHS 
    ~/Library/Frameworks /Library/Frameworks
    /usr/local/include /usr/include
    /sw/include
    /opt/local/include /opt/csw/include /opt/include
    $ENV{HOME}/usr/include $ENV{HOME}/usr/local/include 
    /mingw/include)

find_library( AVCodec_LIBRARIES
	      NAMES avcodec libavcodec.a libavcodec.so
	      HINTS $ENV{AVCODEC_HOME}
	      PATH_SUFFIXES lib64 lib bin
	      PATHS
		/usr/local    /usr	   /sw
		/opt/local    /opt/csw     /opt
		/mingw
    $ENV{HOME}/usr/lib $ENV{HOME}/usr/local/lib64 $ENV{HOME}/usr/local/lib
)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(AVCodec
                                  FOUND_VAR AVCodec_FOUND
                                  REQUIRED_VARS AVCodec_LIBRARIES AVCodec_INCLUDE_DIRS
)
